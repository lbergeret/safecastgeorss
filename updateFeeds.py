#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from optparse import OptionParser
import urllib
import datetime
import uuid

# -----------------------------------------------------------------------------
# Main
# -----------------------------------------------------------------------------
if __name__ == '__main__':
  # Process command line options
  parser = OptionParser("Usage: updateFeeds.py [options]")
  parser.add_option("-r", "--request",
                    type=str, dest="request", default="",
                    help="customized URL request")
  (options, args) = parser.parse_args()

  now = datetime.datetime.utcnow()
  d = datetime.timedelta(minutes = 5)
  before = now - d
  after = now

  if (options.request == ""):
    REQUEST = 'http://api.safecast.org/en-US/measurements?captured_after="%s"&captured_before="%s"&format=csv'
    print REQUEST % (before.strftime("%Y/%m/%d %H:%M:%S"), after.strftime("%Y/%m/%d %H:%M:%S"))
    data = urllib.urlopen(REQUEST % (before.strftime("%Y/%m/%d %H:%M:%S"), after.strftime("%Y/%m/%d %H:%M:%S"))).readlines()
  else:
    REQUEST = options.request
    print REQUEST
    data = urllib.urlopen(REQUEST).readlines()

  for l in data:
    t, lat, lon, value, unit, location, device_id, md5, height, surface, radiation, upload, loader_id = l.strip().split(",")
    try:
      dtime = datetime.datetime.strptime(t, '%Y-%m-%d %H:%M:%S UTC')
    except:
      continue

    output = open("device_%s.csv" % device_id, "a")
    output.write("%s,%s,%s,%s,%s,%s\n" %(device_id, dtime.strftime("%Y-%m-%dT%H:%M:%SZ"),value,lon,lat,uuid.uuid1().urn))
    output.close()
